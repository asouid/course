package fr.formation.course;

import fr.formation.course.bll.CoureurService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseApplicationTests {
    @Autowired
    CoureurService service;

    @Test
    public void contextLoads() {
        service.start();
      //  service.arrivee("2222");
        service.getClassement();
        service.getClassementM();
        service.getClassementN();
    }

}

