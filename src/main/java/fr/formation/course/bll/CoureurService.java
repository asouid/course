package fr.formation.course.bll;

import fr.formation.course.bo.Coureur;

import java.util.List;

public interface CoureurService {
    void start ();
    void arrivee (String dossard);
    List<Coureur> getClassement();
    List<Coureur> getClassementM();
    List<Coureur> getClassementN();
}
