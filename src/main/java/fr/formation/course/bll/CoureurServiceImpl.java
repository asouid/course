package fr.formation.course.bll;

import fr.formation.course.bo.Coureur;
import fr.formation.course.dal.CoureurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CoureurServiceImpl implements CoureurService {
    @Autowired
    CoureurRepository dao;

    @Override
    public void start() {
        List<Coureur> listC = (List<Coureur>) dao.findAll();
        Date d = new Date();
        for (Coureur c : listC) {
            c.sethDepart(d);
            dao.save(c);
        }
    }

    @Override
    public void arrivee(String dossard) {
        Date d = new Date();
        Coureur coureur = dao.findByNumDossard(dossard);
        coureur.sethArrivee(d);
        dao.save(coureur);
    }

    @Override
    public List<Coureur> getClassement() {
        Sort sort = new Sort(Sort.Direction.DESC, "hArrivee");
        List<Coureur> listC = dao.findAll(sort);
       listC.forEach( System.out :: println);
       return listC;
    }

    @Override
    public List<Coureur> getClassementM() {
        Sort sort = new Sort(Sort.Direction.DESC, "hArrivee");
        List<Coureur> listC = dao.findAllBySexe("M",sort);
        listC.forEach( System.out :: println);
        return listC;
    }
    @Override
    public List<Coureur> getClassementN() {
        List<Coureur> listC = dao.findAllByOrderByNomAsc();
        listC.forEach( System.out :: println);
        return listC;
    }
}
