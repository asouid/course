package fr.formation.course.ws;

import fr.formation.course.bll.CoureurService;
import fr.formation.course.bo.Coureur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CoureurWeb {
    @Autowired
    CoureurService service;

    @GetMapping("/depart")
    @CrossOrigin(origins = "http://localhost:4200")
    void start() {
        service.start();
    }

    @GetMapping("/arrivee")
    @CrossOrigin(origins = "http://localhost:4200")
    List<Coureur> arrivee(@RequestParam String dossard) {
        service.arrivee(dossard);
        return service.getClassement();
    }
}
