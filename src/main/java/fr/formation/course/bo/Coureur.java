package fr.formation.course.bo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Coureur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nom;
    private String sexe;
    private Date hDepart;
    private Date hArrivee;
    private String numDossard;

    public Coureur() {
    }

    public Coureur(String nom, String sexe, Date hDepart, Date hArrivee, String numDossard) {
        this.nom = nom;
        this.sexe = sexe;
        this.hDepart = hDepart;
        this.hArrivee = hArrivee;
        this.numDossard = numDossard;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Date gethDepart() {
        return hDepart;
    }

    public void sethDepart(Date hDepart) {
        this.hDepart = hDepart;
    }

    public Date gethArrivee() {
        return hArrivee;
    }

    public void sethArrivee(Date hArrivee) {
        this.hArrivee = hArrivee;
    }

    public String getNumDossard() {
        return numDossard;
    }

    public void setNumDossard(String numDossard) {
        this.numDossard = numDossard;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Coureur{");
        sb.append("id=").append(id);
        sb.append(", nom='").append(nom).append('\'');
        sb.append(", sexe='").append(sexe).append('\'');
        sb.append(", hDepart=").append(hDepart);
        sb.append(", hArrivee=").append(hArrivee);
        sb.append(", numDossard='").append(numDossard).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
