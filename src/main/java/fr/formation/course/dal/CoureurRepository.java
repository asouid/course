package fr.formation.course.dal;

import fr.formation.course.bo.Coureur;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CoureurRepository extends CrudRepository<Coureur,Integer> {
    Coureur findByNumDossard (String numDossard);
    List<Coureur> findAll(Sort sort);
    List<Coureur> findAllBySexe(String sexe, Sort sort);
    List<Coureur> findAllByOrderByNomAsc();
    List<Coureur> findAllBySexeOrderByNumDossardAsc (String sexe);

}
